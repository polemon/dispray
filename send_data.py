#!/usr/bin/env python3

import sys
import pprint
import time
import os
import socket
import serial

from functools import reduce

disp_id = "00"

os.environ['PYTHONUNBUFFERED'] = '1'

def chksum(line):
    chksum = reduce((lambda x,y: x^y), line)
    return '{:x}'.format(chksum)
  
def mkmsgline(mesg):
    address = "<ID" + disp_id + ">"
    end_marker = "<E>"

    byteline = bytearray(address, 'ascii') + mesg + bytearray(end_marker, 'ascii')
    
    return byteline

def prepline(mesg):
    cs = bytearray(chksum(mesg), 'ascii')
    msgline = mkmsgline(mesg + cs)

    return msgline


def main():
    message = ""

    if len(sys.argv) >= 2:
        message = sys.argv[1]

    bytes_message = bytearray(message, 'ascii')
    #bytes_message = bytearray("<FA40>", 'ascii') + bytearray(b'\xFF\xC3\xA5\x99\x99\xA5\xC3\xFF')
    prepped_line = prepline(bytes_message)

    #conn = open("./serial", 'wb')

    #conn = serial.Serial(port = '/dev/ttyUSB0', baudrate = 9600, bytesize = 8, parity = 'N', stopbits = 1)

    #print(conn)

    for i in prepped_line:
        print(bytes([i]), end = ' ', flush = True)
        #conn.write(bytes([i]))
        #conn.flush()
        time.sleep(0.01)


if __name__ == "__main__":
    main()
